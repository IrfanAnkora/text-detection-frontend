const ipc = require('electron').ipcRenderer

ipc.on('imageLoad', (event, args) =>{
  document.getElementById("slika").src = args.imageData;
})

function startScript() {
    var path = document.getElementById("odaberi").files[0].path; 
    ipc.send('start-script', {filePath: path})
  }

  document.getElementById('pokreni').addEventListener('click', () => {
    startScript()
})
